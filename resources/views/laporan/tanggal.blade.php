<table class="table">
	<tr>
		<td>NO</td>
		<td>KODE TRANSAKSI</td>
		<td>NAMA CUSTOMER</td>
		<td>NAMA BARANG</td>
		<td>HARGA JUAL</td>
		<td>JUMLAH BARANG</td>
		<td>JUMLAH PEMBELIAN</td>
	</tr>
	@php $i=1 @endphp
	@foreach($lap as $l)
		<tr>
			<td>{{ $i++ }}</td>
			<td>{{$l->kode_trs}}</td>
			<td>{{$l->nama_cus}}</td>
			<td>{{$l->nama_brg}}</td>
			<td>{{$l->h_jual_brg}}</td>
			<td>{{$l->jml_trs}}</td>
			<td>{{$l->subtotal_trs}}</td>
		</tr>
	@endforeach
</table>