@extends('template.index')

@section('content')
<div class="animated fadeIn">
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <strong>FORM LAPORAN</strong>
                </div>
                <div class="card-body card-block">
                    <form action="/lap/ptgl" method="post" enctype="multipart/form-data" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="row form-group">
                            <div class="col col-md-3"><label class=" form-control-label">PERIODE</label></div>
                            <div class="col-12 col-md-9">
                                <select data-placeholder="Pilih Periode..." class="standardSelect" tabindex="1" name="per">
                                    <option value="" label="default"></option>
                                    <option value="1">Tanggal</option>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label class=" form-control-label">PERIODE</label></div>
                            <div class="col-12 col-md-9">
                                <input class="date form-control" type="text" name="laptgl">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><button type="submit" class="btn btn-primary">CETAK</button></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection