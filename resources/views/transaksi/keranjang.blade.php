<table class="table table-bordered">
    <thead>
        <tr>
            <th scope="col">NO</th>
            <th scope="col">NAMA</th>
            <th scope="col">HARGA</th>
            <th scope="col">JUMLAH</th>
            <th scope="col">TOTAL</th>
            <th scope="col"></th>
        </tr>
    </thead>
    <tbody>
        <?php $i = 0; ?>
        @foreach($krn as $k)                     
            <tr>
                <th scope="row">{{ $i+1 }}</th>
                <td>{{ $k->nama_brg }}</td>
                <td>{{ $k->h_jual_brg }}</td>
                <td>{{ $k->jml_trs }}</td>
                <td>{{ $k->subtotal_trs }}</td>
                <td><input id="idkrn" type="hidden" value="{{ $k->id_krn }}"><a href="#" class="btn btn-danger delkrn">Batal</a></td>
            </tr>
            <?php $i++; ?>
        @endforeach
    </tbody>
</table>
<input type="hidden" id="idtrskrn" value="{{ $k->id_trs }}">
<input type="hidden" id="ttltrskrn" value="{{ $ttl }}">
<table style="width: 100%">
    <tr>
        <td style="width: 90%">TOTAL TRANSAKSI : {{ $ttl }}</td>
        <td style="width: 10%"><a href="#" class="btn btn-success strs">Selesai</a></td>
    </tr>
</table>
<script type="text/javascript">
    $(document).ready(function(){
        $(".delkrn").click(function(){
            var vidkrn = $('#idkrn').val();
            $.ajax({
                type: 'POST',
                url: '/trs/delkrn',
                data: {"_token": "{{ csrf_token() }}",idkrn: vidkrn},
                success: function() {
                    $('.t_krntrs').load("/trs/keranjang");
                }
            });
        });
        $(".strs").click(function(){
            var vidtrskrn = $('#idtrskrn').val();
            var vttltrskrn = $('#ttltrskrn').val();
            $.ajax({
                type: 'POST',
                url: '/trs/strs',
                data: {"_token": "{{ csrf_token() }}",idtrskrn: vidtrskrn,ttltrskrn: vttltrskrn},
                success: function() {
                    window.open('/trs/invoice/'+$('#idtrskrn').val());
                    location.reload();
                }
            });
        });
    });
</script>