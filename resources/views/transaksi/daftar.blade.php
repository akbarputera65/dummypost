@extends('template.index')

@section('content')
<div class="animated fadeIn">
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <strong>TRANSAKSI PENJUALAN</strong>
                </div>
                <div class="card-body card-block">
                    <form action="#" method="post" enctype="multipart/form-data" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">KODE TRANSAKSI</label></div>
                            <div class="col-12 col-md-9"><input type="text" id="kodetrs" name="kodetrs" class="form-control"></div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label class=" form-control-label">NAMA CUSTOMER</label></div>
                            <div class="col-12 col-md-9">
                                <select data-placeholder="Pilih Customer..." class="standardSelect" tabindex="1" name="custrs" id="custrs">
                                    <option value="" label="default"></option>
                                    @foreach($cus as $c)
                                        <option value="{{ $c->id_cus }}">{{ $c->nama_cus }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label class=" form-control-label">NAMA BARANG</label></div>
                            <div class="col-12 col-md-9">
                                <select data-placeholder="Pilih Barang..." class="standardSelect" tabindex="1" name="brgtrs" id="brgtrs" >
                                    <option value="" label="default"></option>
                                    @foreach($brg as $b)
                                        <option value="{{ $b->id_brg }}">{{ $b->nama_brg }} - {{ $b->kode_brg }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="disabled-input" class=" form-control-label">HARGA</label></div>
                            <div class="col-12 col-md-9"><input type="number" name="hrgtrs" id="hrgtrs" value="0" class="form-control" readonly></div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">JUMLAH</label></div>
                            <div class="col-12 col-md-9"><input type="number" name="jmltrs" id="jmltrs" onkeyup="totaltrs()" class="form-control"></div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><label for="disabled-input" class=" form-control-label">TOTAL</label></div>
                            <div class="col-12 col-md-9"><input type="number" name="ttltrs" id="ttltrs" value="0" class="form-control" readonly></div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3"><a href="#" class="btn btn-primary smpntrs">Simpan</a></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">KERANJANG</strong>
                </div>
                <div class="card-body">
                    <div class="t_krntrs">
        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function totaltrs(){
        var a = $('#hrgtrs').val();
        var b = $('#jmltrs').val();
        var c = parseInt(a)*parseInt(b);
        $('#ttltrs').val(c);
    }
</script>
@endsection
