<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">
        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li>
                    <a href="/"><i class="menu-icon fa fa-laptop"></i>Dashboard </a>
                </li>
                <li class="">
                    <a href="/brg/data"> <i class="menu-icon fa fa-list"></i>Barang</a>
                </li>
                <li class="">
                    <a href="/cus/data"> <i class="menu-icon fa fa-list"></i>Customer</a>
                </li>
                <li class="">
                    <a href="/trs/data"> <i class="menu-icon fa fa-plus"></i>Transaksi</a>
                </li>
                <li class="">
                    <a href="/lap/flap"> <i class="menu-icon fa fa-list"></i>Laporan</a>
                </li>
                <li class="">
                    <a href="/opn/data"> <i class="menu-icon fa fa-plus"></i>Stock Opname</a>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside>