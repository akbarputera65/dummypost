<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Bengkel</title>
    <meta name="description" content="ShaynaAdmin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="https://i.imgur.com/QRAUqs9.png">
    <link rel="shortcut icon" href="https://i.imgur.com/QRAUqs9.png">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
    <link rel="stylesheet" href="{{url('shayna/assets/css/cs-skin-elastic.css')}}">
    <link rel="stylesheet" href="{{url('shayna/assets/css/style.css')}}">
    <link rel="stylesheet" href="{{url('shayna/assets/css/lib/datatable/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('shayna/assets/css/lib/chosen/chosen.min.css')}}">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/jqvmap@1.5.1/dist/jqvmap.min.css" rel="stylesheet">
</head>
<body>
    @include('template.sidebar')
    <div id="right-panel" class="right-panel">
        @include('template.header')
        <div class="content">
            @yield('content')
        </div>
        <div class="clearfix"></div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
    <script src="{{url('shayna/assets/js/main.js')}}"></script>
    <script src="{{url('shayna/assets/js/lib/data-table/datatables.min.js')}}"></script>
    <script src="{{url('shayna/assets/js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{url('shayna/assets/js/lib/data-table/dataTables.buttons.min.js')}}"></script>
    <script src="{{url('shayna/assets/js/lib/data-table/buttons.bootstrap.min.js')}}"></script>
    <script src="{{url('shayna/assets/js/lib/data-table/jszip.min.js')}}"></script>
    <script src="{{url('shayna/assets/js/lib/data-table/vfs_fonts.js')}}"></script>
    <script src="{{url('shayna/assets/js/lib/data-table/buttons.html5.min.js')}}"></script>
    <script src="{{url('shayna/assets/js/lib/data-table/buttons.print.min.js')}}"></script>
    <script src="{{url('shayna/assets/js/lib/data-table/buttons.colVis.min.js')}}"></script>
    <script src="{{url('shayna/assets/js/init/datatables-init.js')}}"></script>
    <script src="{{url('shayna/assets/js/lib/chosen/chosen.jquery.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.7.3/dist/Chart.bundle.min.js"></script>
    <script src="{{url('shayna/assets/js/init/chartjs-init.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery.flot@0.8.3/jquery.flot.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flot-spline@0.0.1/js/jquery.flot.spline.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#bootstrap-data-table-export').DataTable();
        } );
    </script>
    <script>
        jQuery(document).ready(function() {
            jQuery(".standardSelect").chosen({
                disable_search_threshold: 10,
                no_results_text: "Oops, nothing found!",
                width: "100%"
            });
        });
    </script>
    <script type="text/javascript">
        $("#brgtrs").change(function() {
            $.ajax({
                url: '/trs/gethrgbrg/' + $(this).val(),
                type: 'get',
                data: {},
                dataType: 'json',
                success: function(data) {
                    if (data.success == true) {
                        $("#hrgtrs").val(data.info);
                    } else {
                        $("#hrgtrs").val('');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {}
            });
        }); 
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(".smpntrs").click(function(){
                var vkodetrs = $('#kodetrs').val();
                var vcustrs = $('#custrs').val();
                var vbrgtrs = $('#brgtrs').val();
                var vjmltrs = $('#jmltrs').val();
                var vttltrs = $('#ttltrs').val();
                var vtgltrs = $('#tgltrs').val();
                $.ajax({
                    type: 'POST',
                    url: '/trs/tambah',
                    data: {"_token": "{{ csrf_token() }}",kodetrs: vkodetrs, custrs: vcustrs, brgtrs: vbrgtrs, jmltrs: vjmltrs, ttltrs: vttltrs},
                    success: function() {
                        $('.t_krntrs').load("/trs/keranjang");
                    }
                });
            });
        });
    </script>
    
<script type="text/javascript">
    $('.date').datepicker({  
       format: 'yyyy-mm-dd'
     });  
</script>
</body>
</html>