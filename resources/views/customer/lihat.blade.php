@extends('template.index')

@section('content')
<div class="animated fadeIn">
    <div class="row">
		<div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <table>
                        <tr>
                            <td style="width: 90%"><strong class="card-title">DATA CUSTOMER</strong></td>
                            <td style="width: 10%"><button type="button" class="btn btn-primary mb-1" data-toggle="modal" data-target="#tambahModal">TAMBAH</button></td>
                        </tr>
                    </table>
                    <div class="modal fade" id="tambahModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <form action="/cus/tambah" method="post" enctype="multipart/form-data" class="form-horizontal">
                                    {{ csrf_field() }}
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">KODE</label></div>
                                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="tkode" class="form-control"></div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">NAMA</label></div>
                                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="tnama" class="form-control"></div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">ALAMAT</label></div>
                                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="talamat" class="form-control"></div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">NO. TELEPON</label></div>
                                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="tnotelp" class="form-control"></div>
                                        </div>     
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-primary">SIMPAN</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table id="bootstrap-data-table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
            	                <th>KODE</th>
                                <th>NAMA</th>
                                <th>ALAMAT</th>
                                <th>NO. TELEPON</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        	@foreach($cus as $c)
	                            <tr>
	                                <td>{{ $c->kode_cus }}</td>
	                                <td>{{ $c->nama_cus }}</td>
	                                <td>{{ $c->alamat_cus }}</td>
	                                <td>{{ $c->notelp_cus }}</td>
	                                <td><button type="button" class="btn btn-primary mb-1" data-toggle="modal" data-target="#mediumModal{{ $c->id_cus }}">EDIT</button> <a href="/cus/hapus/{{ $c->id_cus }}" class="btn btn-danger">HAPUS</a></td>
	                            </tr>
                                <div class="modal fade" id="mediumModal{{ $c->id_cus }}" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <form action="/cus/edit/{{ $c->id_cus }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                                {{ csrf_field() }}
                                                {{ method_field('PUT') }}
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row form-group">
                                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">KODE</label></div>
                                                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="ekode" class="form-control" value="{{ $c->kode_cus }}"></div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">NAMA</label></div>
                                                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="enama" class="form-control" value="{{ $c->nama_cus }}"></div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">ALAMAT</label></div>
                                                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="ealamat" class="form-control" value="{{ $c->alamat_cus }}"></div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">NO. TELEPON</label></div>
                                                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="enotelp" class="form-control" value="{{ $c->notelp_cus }}"></div>
                                                    </div>     
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" class="btn btn-primary">SIMPAN</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
	                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection