@extends('template.index')

@section('content')
<div class="animated fadeIn">
    <div class="row">
		<div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">DATA BARANG</strong>
                </div>
                <div class="card-body">
                    <table id="bootstrap-data-table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
            	                <th>KODE</th>
                                <th>NAMA</th>
                                <th>STOK</th>
                                <th>STOK GUDANG</th>
                                <th>SELISIH</th>
                                <th>KETERANGAN</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        	@foreach($brg as $b)
	                            <tr>
	                                <td>{{ $b->kode_brg }}</td>
	                                <td>{{ $b->nama_brg }}</td>
	                                <td>{{ $b->stok_brg }}</td>
	                                <td>{{ $jmlgdg = \App\Models\Opname::where(['id_brg' => $b->id_brg])->pluck('jml_opn')->first() }}</td>
	                                <td>{{ $jmlgdg-$b->stok_brg }}</td>
                                    <td>{{ \App\Models\Opname::where(['id_brg' => $b->id_brg])->pluck('ket_opn')->first() }}</td>
	                                <td>
                                        <button type="button" class="btn btn-primary mb-1" data-toggle="modal" data-target="#opnModal{{ $b->id_brg }}">INPUT</button>
                                    </td>
	                            </tr>
                                <div class="modal fade" id="opnModal{{ $b->id_brg }}" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <form action="/opn/tambah/{{ $b->id_brg }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="idopn" value="{{ \App\Models\Opname::where(['id_brg' => $b->id_brg])->pluck('id_opn')->first() }}">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row form-group">
                                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">STOK GUDANG</label></div>
                                                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="jmlopn" class="form-control" value=""></div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">KETERANGAN</label></div>
                                                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="ketopn" class="form-control" value=""></div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" class="btn btn-primary">SIMPAN</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
	                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection