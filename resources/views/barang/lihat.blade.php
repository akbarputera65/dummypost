@extends('template.index')

@section('content')
<div class="animated fadeIn">
    <div class="row">
		<div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <table>
                        <tr>
                            <td style="width: 90%"><strong class="card-title">DATA BARANG</strong></td>
                            <td style="width: 10%"><button type="button" class="btn btn-primary mb-1" data-toggle="modal" data-target="#tambahModal">TAMBAH</button></td>
                        </tr>
                    </table>
                    <div class="modal fade" id="tambahModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <form action="/brg/tambah" method="post" enctype="multipart/form-data" class="form-horizontal">
                                    {{ csrf_field() }}
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">KODE</label></div>
                                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="tkode" class="form-control"></div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">NAMA</label></div>
                                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="tnama" class="form-control"></div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">STOK</label></div>
                                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="tstok" class="form-control"></div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">HARGA MODAL</label></div>
                                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="thmodal" class="form-control"></div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-3"><label for="text-input" class=" form-control-label">HARGA JUAL</label></div>
                                            <div class="col-12 col-md-9"><input type="text" id="text-input" name="thjual" class="form-control"></div>
                                        </div>  
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-primary">SIMPAN</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table id="bootstrap-data-table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
            	                <th>KODE</th>
                                <th>NAMA</th>
                                <th>STOK</th>
                                <th>HARGA MODAL</th>
                                <th>HARGA JUAL</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        	@foreach($brg as $b)
	                            <tr>
	                                <td>{{ $b->kode_brg }}</td>
	                                <td>{{ $b->nama_brg }}</td>
	                                <td>{{ $b->stok_brg }}</td>
	                                <td>{{ $b->h_modal_brg }}</td>
	                                <td>{{ $b->h_jual_brg }}</td>
	                                <td><button type="button" class="btn btn-primary mb-1" data-toggle="modal" data-target="#mediumModal{{ $b->id_brg }}">EDIT</button> <a href="/brg/hapus/{{ $b->id_brg }}" class="btn btn-danger">HAPUS</a></td>
	                            </tr>
                                <div class="modal fade" id="mediumModal{{ $b->id_brg }}" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <form action="/brg/edit/{{ $b->id_brg }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                                {{ csrf_field() }}
                                                {{ method_field('PUT') }}
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row form-group">
                                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">KODE</label></div>
                                                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="ekode" class="form-control" value="{{ $b->kode_brg }}"></div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">NAMA</label></div>
                                                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="enama" class="form-control" value="{{ $b->nama_brg }}"></div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">STOK</label></div>
                                                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="estok" class="form-control" value="{{ $b->stok_brg }}"></div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">HARGA MODAL</label></div>
                                                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="ehmodal" class="form-control" value="{{ $b->h_modal_brg }}"></div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col col-md-3"><label for="text-input" class=" form-control-label">HARGA JUAL</label></div>
                                                        <div class="col-12 col-md-9"><input type="text" id="text-input" name="ehjual" class="form-control" value="{{ $b->h_jual_brg }}"></div>
                                                    </div>        
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" class="btn btn-primary">SIMPAN</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
	                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection