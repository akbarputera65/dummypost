<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model{
    use HasFactory;
    protected $table = "transaksi";
    protected $primaryKey = 'id_trs';
    protected $fillable = ['id_trs','id_cus','kode_trs','tgl_trs','total_trs'];
}