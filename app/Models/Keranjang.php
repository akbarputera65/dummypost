<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Keranjang extends Model{
    use HasFactory;
    protected $table = "keranjang";
    protected $primaryKey = 'id_krn';
    protected $fillable = ['id_krn','id_trs','id_brg','jml_trs','subtotal_trs'];
}