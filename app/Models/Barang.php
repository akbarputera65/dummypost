<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Barang extends Model{
    use HasFactory;
    protected $table = "barang";
    protected $primaryKey = 'id_brg';
    protected $fillable = ['id_brg','kode_brg','nama_brg','stok_brg','h_modal_brg','h_jual_brg'];
    public function customer(){
        return $this->belongsToMany('App\Models\Customer');
    }
    public function opname(){
        return $this->hasOne('App\Models\Opname');
    }
}