<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model{
    use HasFactory;
    protected $table = "customer";
    protected $primaryKey = 'id_cus';
    protected $fillable = ['id_cus','kode_cus','nama_cus','alamat_cus','notelp_cus'];
    public function barang(){
        return $this->belongsToMany('App\Models\Barang');
    }
}