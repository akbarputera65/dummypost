<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Opname extends Model{
    use HasFactory;
    protected $table = "opname";
    protected $primaryKey = 'id_opn';
    protected $fillable = ['id_opn','id_brg','jml_opn','ket_opn'];
    public function barang(){
        return $this->belongsTo('App\Models\Barang');
    }
}