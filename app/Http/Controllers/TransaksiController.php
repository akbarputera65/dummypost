<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;
use App\Models\Barang;
use App\Models\Customer;
use App\Models\Transaksi;
use App\Models\Keranjang;
use PDF;

class TransaksiController extends Controller{
    public function index(){
        $cus = Customer::get();
        $brg = Barang::get();
        return view('transaksi/daftar', ['cus' => $cus, 'brg' => $brg]);
    }
    public function gethrgbrg($id){
        $fill = DB::table('barang')->where('id_brg', $id)->pluck('h_jual_brg');
        return Response::json(['success'=>true, 'info'=>$fill]);
    }
    public function tambah(Request $request){
        $kdtrs = DB::table('transaksi')->where('kode_trs', $request->kodetrs)->count();
        if ($kdtrs == 0) {
            Transaksi::create([
                'id_cus' => $request->custrs,
                'kode_trs' => $request->kodetrs,
                'tgl_trs' => date('Y-m-d'),
                'total_trs' => $request->ttltrs
            ]);
            $trs = Transaksi::orderBy('id_trs', 'DESC')->take(1)->get();
            foreach ($trs as $atrs) {
                $idtrs = $atrs['id_trs'];
            }
            Keranjang::create([
                'id_trs' => $idtrs,
                'id_brg' => $request->brgtrs,
                'jml_trs' => $request->jmltrs,
                'subtotal_trs' => $request->ttltrs
            ]);
        }else{
            $trs = Transaksi::orderBy('id_trs', 'DESC')->take(1)->get();
            foreach ($trs as $atrs) {
                $idtrs = $atrs['id_trs'];
            }
            Keranjang::create([
                'id_trs' => $idtrs,
                'id_brg' => $request->brgtrs,
                'jml_trs' => $request->jmltrs,
                'subtotal_trs' => $request->ttltrs
            ]);
        }     
    }
    public function keranjang(){
        $trs = Transaksi::orderBy('id_trs', 'DESC')->take(1)->get();
        foreach ($trs as $atrs) {
            $idtrs = $atrs['id_trs'];
        }
        $krn = DB::table('keranjang as a')->select('a.jml_trs','a.subtotal_trs','b.nama_brg','b.h_jual_brg','a.id_krn','a.id_trs')->join('barang as b', 'b.id_brg', '=', 'a.id_brg')->where('id_trs', $idtrs)->get();
        $ttl = DB::table('keranjang')->where('id_trs', '=', $idtrs)->sum('subtotal_trs');
        return view('transaksi/keranjang', ['krn' => $krn, 'ttl' => $ttl]);
    }
    public function delkrn(Request $request){
        $krn = Keranjang::find($request->idkrn);
        $krn->delete();
    }
    public function s_trs(Request $request){
        $trs = Transaksi::find($request->idtrskrn);
        $trs->total_trs = $request->ttltrskrn;
        $trs->save();
    }
    public function invoice($id){
        $inv = DB::table('keranjang as a')->select('b.kode_brg','b.nama_brg','a.jml_trs','b.h_jual_brg','a.subtotal_trs')->join('barang as b', 'b.id_brg', '=', 'a.id_brg')->where('id_trs', $id)->get();
        $ttl = DB::table('keranjang')->where('id_trs', '=', $id)->sum('subtotal_trs');
        $pdf = PDF::loadview('laporan/invoice',['inv'=>$inv,'ttl'=>$ttl]);
        return $pdf->stream();
    }
    public function f_lap(){
        return view('laporan/form');
    }
    public function p_tgl(Request $request){
        $lap = DB::table('keranjang as a')->select('b.kode_trs','d.nama_cus','c.nama_brg','c.h_jual_brg','a.jml_trs','a.subtotal_trs')->join('transaksi as b', 'a.id_trs', '=', 'b.id_trs')->join('barang as c', 'a.id_brg', '=', 'c.id_brg')->join('customer as d', 'b.id_cus', '=', 'd.id_cus')->where('tgl_trs', $request->laptgl)->get();
        $pdf = PDF::loadview('laporan/tanggal',['lap'=>$lap]);
        return $pdf->stream();
    }
}