<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Barang;
use App\Models\Opname;

class OpnameController extends Controller{
    public function index(){
        $brg = Barang::get();
        return view('opname/data', ['brg' => $brg]);
    }
    public function tambah($id,Request $request){
        $copn = DB::table('opname')->where('id_brg', $id)->count();
        if ($copn == 0) {
            Opname::create([
                'id_brg' => $id,
                'jml_opn' => $request->jmlopn,
                'ket_opn' => $request->ketopn
            ]);
        }else{
            $opn = Opname::find($request->idopn);
            $opn->jml_opn = $request->jmlopn;
            $opn->ket_opn = $request->ketopn;
            $opn->save();
        }     
    }
}