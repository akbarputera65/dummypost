<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Customer;

class CustomerController extends Controller{
    public function lihat(){
        $cus = Customer::all();
        return view('customer/lihat', ['cus' => $cus]);
    }
    public function tambah(Request $request){
        $this->validate($request,[
            'tkode' => 'required',
            'tnama' => 'required',
            'talamat' => 'required',
            'tnotelp' => 'required'
        ]);
        Customer::create([
            'kode_cus' => $request->tkode,
            'nama_cus' => $request->tnama,
            'alamat_cus' => $request->talamat,
            'notelp_cus' => $request->tnotelp
        ]);
        return redirect('/cus/data');
    }
    public function edit($id_cus, Request $request){
        $this->validate($request,[
            'ekode' => 'required',
            'enama' => 'required',
            'ealamat' => 'required',
            'enotelp' => 'required'
        ]);
        $cus = Customer::find($id_cus);
        $cus->kode_cus = $request->ekode;
        $cus->nama_cus = $request->enama;
        $cus->alamat_cus = $request->ealamat;
        $cus->notelp_cus = $request->enotelp;
        $cus->save();
        return redirect('/cus/data');
    }
    public function hapus($id_cus){
        $cus = Customer::find($id_cus);
        $cus->delete();
        return redirect('/cus/data');
    }
}
