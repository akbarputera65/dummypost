<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Barang;

class BarangController extends Controller{
    public function lihat(){
        $brg = Barang::all();
        return view('barang/lihat', ['brg' => $brg]);
    }
    public function tambah(Request $request){
        $this->validate($request,[
            'tkode' => 'required',
            'tnama' => 'required',
            'tstok' => 'required',
            'thmodal' => 'required',
            'thjual' => 'required'
        ]);
        Barang::create([
            'kode_brg' => $request->tkode,
            'nama_brg' => $request->tnama,
            'stok_brg' => $request->tstok,
            'h_modal_brg' => $request->thmodal,
            'h_jual_brg' => $request->thjual
        ]);
        return redirect('/brg/data');
    }
    public function edit($id_brg, Request $request){
        $this->validate($request,[
            'ekode' => 'required',
            'enama' => 'required',
            'estok' => 'required',
            'ehmodal' => 'required',
            'ehjual' => 'required'
        ]);
        $brg = Barang::find($id_brg);
        $brg->kode_brg = $request->ekode;
        $brg->nama_brg = $request->enama;
        $brg->stok_brg = $request->estok;
        $brg->h_modal_brg = $request->ehmodal;
        $brg->h_jual_brg = $request->ehjual;
        $brg->save();
        return redirect('/brg/data');
    }
    public function hapus($id_brg){
        $brg = Barang::find($id_brg);
        $brg->delete();
        return redirect('/brg/data');
    }
}