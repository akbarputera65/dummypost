<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BarangController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\TransaksiController;
use App\Http\Controllers\OpnameController;
use App\Http\Controllers\DashboardController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('/dashboard');
});
Route::get('/', [DashboardController::class, 'index']);

Route::get('/brg/data', [BarangController::class, 'lihat']);
Route::post('/brg/tambah', [BarangController::class, 'tambah']);
Route::put('/brg/edit/{id_brg}', [BarangController::class, 'edit']);
Route::get('/brg/hapus/{id_brg}', [BarangController::class, 'hapus']);

Route::get('/cus/data', [CustomerController::class, 'lihat']);
Route::post('/cus/tambah', [CustomerController::class, 'tambah']);
Route::put('/cus/edit/{id_cus}', [CustomerController::class, 'edit']);
Route::get('/cus/hapus/{id_cus}', [CustomerController::class, 'hapus']);

Route::get('/trs/data', [TransaksiController::class, 'index']);
Route::get('/trs/gethrgbrg/{id}', [TransaksiController::class, 'gethrgbrg']);
Route::post('/trs/tambah', [TransaksiController::class, 'tambah']);
Route::get('/trs/keranjang', [TransaksiController::class, 'keranjang']);
Route::post('/trs/delkrn', [TransaksiController::class, 'delkrn']);
Route::post('/trs/strs', [TransaksiController::class, 's_trs']);
Route::get('/trs/invoice/{id}', [TransaksiController::class, 'invoice']);

Route::get('/lap/flap', [TransaksiController::class, 'f_lap']);
Route::post('/lap/ptgl', [TransaksiController::class, 'p_tgl']);

Route::get('/opn/data', [OpnameController::class, 'index']);
Route::post('/opn/tambah/{id}', [OpnameController::class, 'tambah']);